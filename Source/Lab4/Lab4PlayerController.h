// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LAB4_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ALab4PlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* InteractAction;

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	void OnMovePressed(const FInputActionValue& Value);
	void OnJumpPressed();
	void OnInteractPressed();
	
};
