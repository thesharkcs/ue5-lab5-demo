// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"
#include "GameFramework/CharacterMovementComponent.h" 
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true);
    CameraBoom->TargetArmLength = 500.f;
    CameraBoom->SetRelativeRotation(FRotator(-20.f, 0.f, -0.f));
    CameraBoom->bDoCollisionTest = false;

    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false;

	InteractionLength = 3.0f;
}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
}

void ALab4Character::BeginInteraction()
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Interaction Started"));
	GetWorldTimerManager().SetTimer(InteractionTimerHandle, this, &ALab4Character::EndInteraction, InteractionLength, false);
}

void ALab4Character::EndInteraction() 
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Interaction Ended"));
	UpdateActionState(ECharacterActionStateEnum::IDLE);
}

bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	switch (CharacterActionState)
	{
	case ECharacterActionStateEnum::IDLE:
		return true;
		break;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction != ECharacterActionStateEnum::INTERACT) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::INTERACT:
		return false;
		break;
	}

	return false;
}

void ALab4Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
	if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE) {
		if (GetVelocity().Size() <= 0.01f) {
			CharacterActionState = ECharacterActionStateEnum::IDLE;
		}
		else 
		{
			CharacterActionState = ECharacterActionStateEnum::MOVE;
		}
	} 
	else
	{
		CharacterActionState = newAction;
	}
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    
	if (CanPerformAction(ECharacterActionStateEnum::IDLE))
		UpdateActionState(ECharacterActionStateEnum::IDLE);
}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

