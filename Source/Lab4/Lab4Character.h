// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8 {
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class LAB4_API ALab4Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab4Character();

    FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
        
	UFUNCTION(BlueprintImplementableEvent)
	void MoveEvent(FVector2D Input);

	UFUNCTION(BlueprintImplementableEvent)
	void InteractEvent();

	UFUNCTION(BlueprintImplementableEvent)
	void JumpEvent();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* CameraComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;
    
    
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ECharacterActionStateEnum CharacterActionState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float InteractionLength;

	UFUNCTION(BlueprintCallable)
		void BeginInteraction();

	UFUNCTION(BlueprintCallable)
		void EndInteraction();

	UFUNCTION(BlueprintCallable)
		bool CanPerformAction(ECharacterActionStateEnum updatedAction);

	UFUNCTION(BlueprintCallable)
		void UpdateActionState(ECharacterActionStateEnum newAction);

private:
	FTimerHandle InteractionTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
