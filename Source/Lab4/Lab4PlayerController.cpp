// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4PlayerController.h"
#include "Lab4Character.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

ALab4PlayerController::ALab4PlayerController()
{
}

void ALab4PlayerController::BeginPlay()
{
    Super::BeginPlay();

    //Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void ALab4PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    // Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
        // Setup keyboard inputs
        EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ALab4PlayerController::OnMovePressed);
        EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ALab4PlayerController::OnJumpPressed);
        EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &ALab4PlayerController::OnInteractPressed);

	}
}

void ALab4PlayerController::OnMovePressed(const FInputActionValue& Value)
{
    FVector2D MovementVector = Value.Get<FVector2D>();

    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->MoveEvent(MovementVector);
    }
}

void ALab4PlayerController::OnJumpPressed()
{
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->JumpEvent();
    }
}

void ALab4PlayerController::OnInteractPressed()
{
    ALab4Character * character = Cast<ALab4Character>(this->GetCharacter());
    if (character) {
        character->InteractEvent();
    }
}
