// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab4GameModeBase.h"
#include "Lab4PlayerController.h"

ALab4GameModeBase::ALab4GameModeBase() {
	//PlayerControllerClass = ALab4PlayerController::StaticClass();

    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_Lab4Character"));
	if (PlayerPawnBPClass.Class)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	
    static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/BP_Lab4PlayerController"));
	if (PlayerControllerBPClass.Class)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}

}
